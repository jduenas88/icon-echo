<?php
namespace IconEcho;
use Timber\Menu as TimberMenu;
use Timber\Timber;

class Website{
    protected   $context;
    public      $cache = false,
                $args = [],
                $debug = true;

    function __construct($args = []){
        $this->args = $args;
        add_theme_support('post-formats');
        add_theme_support('post-thumbnails');
        add_theme_support('menus');
        add_theme_support('html5', array('comment-list', 'comment-form', 'search-form', 'gallery', 'caption'));
        add_filter('timber_context', [$this, 'buildContext']);

        if($this->debug){
            echo "<pre>" . print_r($this->context, 1) . "</pre>";
        }
    }

    /**
     * Builds up the Context array
     */
    public function buildContext(){
        Timber::$dirname = 'assets/views';
        $this->context = Timber::get_context();
        $this->context['ajax_url'] = admin_url('admin-ajax.php');
        $this->context['menus'] = $this->getTimberMenus();
        //$this->context['sitewide_options'] = get_fields('options');

        return $this->context;
    }

    /**
     * @return array
     * Iterates through list of menus, creates TimberMenu objects out of them,
     * and returns array of objects created.
     */
    protected function getTimberMenus(){
        $menus = [];
        foreach($this->args['menus'] as $location => $description){
            $menus[] = new TimberMenu($location);
        }

        return $menus;
    }
}