<?php
use Timber\Timber;

$context = Timber::get_context();
$context['post'] = new \Timber\Post();

Timber::render( __DIR__ . '/assets/views/pages/home.twig', $context);