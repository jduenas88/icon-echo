<?php
use Timber\Timber;

$context = Timber::get_context();
$context['posts'] = new \Timber\PostQuery();

Timber::render( __DIR__ . '/assets/views/pages/404.twig', $context);