<?php

$loader = require_once(__DIR__ . '/vendor/autoload.php');
$loader->addPsr4('IconEcho\\', __DIR__ . '/classes/');
require 'functions/menus.php';

$website = new \IconEcho\Website([
    'menus' => getMenus()
]);
