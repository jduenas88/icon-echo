<?php
use Timber\Timber;

$context = Timber::get_context();
$context['posts'] = new \Timber\PostQuery();

Timber::render( __DIR__ . '/assets/views/pages/home.twig', $context);