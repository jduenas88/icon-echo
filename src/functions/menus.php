<?php

/**
 * @return array
 * Returns list of menus
 */
function getMenus(){
    return [
        "primary-nav" => "Primary Navigation",
        "footer-nav" => "Footer Navigation"
    ];
}

/**
 * Registers menus
 */
function iconEchoRegisterMenus(){
    $nav_menus = getMenus();
    register_nav_menus($nav_menus);
}

add_action('init', 'iconEchoRegisterMenus', 10);