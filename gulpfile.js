// Variables
var dotenv = require('dotenv').config(),
    themeName = process.env.THEME_NAME,
    authorName = process.env.THEME_AUTHOR,
    themeVersion = process.env.THEME_VERSION,
    gulp = require('gulp'),
    folders = {
        src: 'src/',
        dist: 'dist/wp-content/themes/' + themeName + '/'
    },
    dep = {
        newer: require('gulp-newer'),
        imageMin: require('gulp-imagemin'),
        sass: require('gulp-sass'),
        cleanCSS: require('gulp-clean-css'),
        sourceMaps: require('gulp-sourcemaps'),
        autoPrefixer: require('gulp-autoprefixer'),
        concat: require('gulp-concat'),
        uglify: require('gulp-uglify'),
        nsg: require('node-sprite-generator'),
        replace: require('gulp-string-replace')
    };

// Tasks


/**
 * gulp finalize-images
 * 1. Takes all images inside `src/images` and compresses them via imagemin
 * 2. Outputs compressed images `to app/images`
 */
gulp.task('finalize-images', ['generate-sprites'], function(){
    var output = folders.dist + 'assets/images/';
    return gulp.src(folders.src + 'assets/images/**/*')
        .pipe(dep.newer(output))
        .pipe(dep.imageMin({optimizationLevel: 5}))
        .pipe(gulp.dest(output));
});

gulp.task('generate-sprites', function(){
    var output = folders.dist + 'assets/images/';

    dep.nsg({
        src: [
            folders.src + 'assets/images/sprites/*.png'
        ],
        spritePath: folders.dist + 'assets/images/spritesheet.png',
        stylesheet: 'scss',
        stylesheetOptions: {
            prefix: '',
            spritePath: 'assets/images/spritesheet.png'
        },
        stylesheetPath: folders.src + 'assets/css/modules/_sprites.scss',
        layout: 'packed',
        layoutOptions: {
          padding: 10
        }
    }, function(err){
        console.log('Spritemap Error: ' + err);
    })
});

/**
 * gulp update-theme-comments
 * Sets the author name, theme name, etc. on WordPress theme before compiling SCSS
 */
gulp.task('update-theme-comments', function(){
   gulp.src([folders.src + 'assets/css/modules/_theme-comments.scss'])
       .pipe(dep.replace('<PLACEHOLDER__THEMENAME>', themeName))
       .pipe(dep.replace('<PLACEHOLDER__THEMEAUTHOR>', authorName))
       .pipe(dep.replace('<PLACEHOLDER__THEMEVERSION>', themeVersion))
       .pipe(gulp.dest(folders.src + 'assets/css/modules/'))
});

/**
 * gulp sass
 * 1. Takes src/style.scss and compiles it into app/style.css
 * 2. Outputs any errors.
 */
gulp.task('sass', ['update-theme-comments'], function(){
    return gulp.src(folders.src + 'assets/css/style.scss')
        .pipe(dep.sourceMaps.init())
        .pipe(dep.sass({outputStyle: 'compressed'}).on('error', dep.sass.logError))
        .pipe(dep.sourceMaps.write('.'))
        .pipe(gulp.dest(folders.dist))
});

/**
 * gulp finalize-css
 * 1. Creates a sourcemap appended to style.css
 * 2. Adds autoprefixing to style.css
 * 3. Minimizes CSS down to a single file (2 for sourcemap)
 * 4. Outputs cleaned, final style.css file to app directory
 */
gulp.task('finalize-css', ['sass'], function(){
    return gulp.src(folders.src + 'style.css')
        .pipe(dep.autoPrefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(dep.cleanCSS())
        .pipe(gulp.dest(folders.dist))
});

/**
 * gulp finalize-js
 * 1. Concatenates all .js files in src/js/ into a single file (all.js)
 */
gulp.task('finalize-js', function(){
    var output = folders.dist;
    return gulp.src([
            // folders.src + 'assets/js/jquery.featherlight.js',
    ])
        .pipe(dep.concat('all.js'))
        .pipe(dep.uglify())
        .pipe(gulp.dest(output))
});

/**
 * gulp build
 * Runs when ./start.sh is run and when `gulp watch` detects a change.
 * 1. Runs `gulp finalize-images`
 * 2. Runs `gulp finalize-css`
 * 3. Runs `gulp finalize-js`
 * 4. Dumps all outputted files to app/
 */
gulp.task('build', ['finalize-images', 'finalize-css', 'finalize-js'], function(){
    var output = folders.dist;
    return gulp.src(folders.src + '**/*')
        .pipe(dep.newer(output))
        .pipe(gulp.dest(output))
});

/**
 * gulp watch
 * Watches for any changes and triggers tasks as needed.
 */
gulp.task('watch', function(){
   var watchList = [
     folders.src + '**/*',
     '!' + folders.src + 'style.css', //ignore style.css
     '!' + folders.src + 'all.js' //ignore all.js
   ];

    gulp.watch(watchList, ['build']);
});