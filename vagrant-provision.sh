#!/usr/bin/env bash

cd /var/www/public

#WP CLI argument values are pulled from the Vagrantfile, which in turn pulls them from .env file.
#Bash vars start at 1 because 0 is the command itself!!!
wp core download
wp core config --dbname="${1}" --dbuser="${3}" --dbpass="${4}" --dbhost="${2}" --dbprefix="${5}"
wp core install --url="${6}" --title="${7}" --admin_user="${9}" --admin_password="${10}" --admin_email="${11}"
#Install ACF
wp plugin install "${12}${13}" --activate
wp plugin install bulkpress --activate
wp plugin install debug-bar --activate
wp plugin install https://github.com/timber/debug-bar-timber/archive/master.zip --activate
